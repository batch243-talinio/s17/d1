// console.log("Hello World!");

// [Section]
	// Functions in js are lines/blocks of codes that tell our device to perform a certain task when called or invoke.
	// functions are mostly created to create a complicated tasks to run several lines of code in succession.
	// it is also used to prevent repeating lines/blocks of cods that perform the same task or function.

// Function declaration
	// function statements - defines a function woth specified parameters.

	/*
		Syntax:
			function functionName(){
				code block(statements)
			}
		*/

		// function keyword - used to define a js function.
		// functionName - functions are name to be able to use it later in the code.
		// function block {} - statements w/c comprise the body of the function. It is where the code will be executed.

	function printName(){
		console.log("My first name is Rudeus!");
		console.log("My last name is Greyrat!");
	}

	// Function Invocation
		// the code block and statements inside a function is not immediately executed when the function is defined or declared.
		// commonly reffered to as "call a function" instead of "invoke a function".

		// Invoking a funtion / calling.

		printName();

// [Section] Function Declaration and Function Expression
	// function can be created through function declaration (e.g. function functionName() {...})

	// declared functions are not executed immediately.

		declaredFunction();

		function declaredFunction(){
			console.log("Hello World from the other declaredFunction!");
		}

	// Funtion expression
		// a can also be stored inside a variable which is called a Function expression.

		let variableFunction = function() {
			console.log("Hello from the other variableFunction!");
		}
		variableFunction();

		let funcExpression = function funcName() {
			console.log("Hello from the other funcExpression!");
		}
		funcExpression();

	// you can reassign a declared function or a function expression

		declaredFunction = function() {
			console.log("Updated declaredFunction!");
		}
		declaredFunction();

		funcExpression = function() {
			console.log("Updated FuncExpression!");
		}
		funcExpression();

		// function expression using const keyword

		const funcExpression2 = function() {
			console.log("Initialized with const! 2");
		}
		funcExpression2();

// [Section] Function Scoping

/*
	Scope is accessibility of variable within out program.

	Js Variables has 3 types of scope.
	1. local/block scope
	2. global scope
	3. function scope
*/

	// 1. local scope
		{
			let myName  = "John Rafael";
			// will still run w/ or w/o calling it but can only be used inside it's own block
		}

	// 2. global scope
		let myNameGlobal = "Rudeus Greyrat";
		// can be used anywhere in the code.

	// 3. function scope
		funcExpression = function() {
			console.log("Updated FuncExpression!");
			// cannot be accessed outside its block but can only be called or invoked.
		}

	function showNames(){
		let functionLet = "John ";
		const functionConst = "Rafael";

		console.log(functionLet);
		console.log(functionConst);
	}

	showNames();

// [Section] Nested Functions
	// you can create another function inside a function. This is called a nested function.

	function myNewFunction() {
		let name = "Kagenouh!";
		console.log(name);

		function myNestedFunction() {
			let nestedName = "Jeminah!";

			console.log(name);
			console.log(nestedName);
		}
		myNestedFunction()
	}
	
	myNewFunction();

	// Function and Global Scope Variables

	let globalName = "Fawer!";

		function myGlobalFunction() {
			let mameGlobal = "Feiler!";

			console.log(mameGlobal);
			console.log(globalName);
			
		}
		myGlobalFunction();

// [Section] Using Alert
	// alert() allows us to show a small window at the top of our browser.
		alert("Hello World!"); // this'll prompt immediately when page loads.

		// Syntax:
			// alert("message alert");


		function myAlert() {
			alert("My function Alert!");
		}
		// myAlert();

		// *Note on using alert()
			// show only an alert for short dialogs/ messages to the user.
			// do not overuse alert.

// [Section] Prompt
		// prompt allows us to show a small window at the top of the browser to gather user input. User inputs are then returned as a string once the prompt is closed. 

		let samplePromt = prompt("Enter your name: ");
		console.log(samplePromt);

		// Syntax:
			// prompt("...");

		let sampleNullPromt = prompt("Dont type anything: ");
		console.log(sampleNullPromt);
		console.log(typeof sampleNullPromt);

		function printWelcomeMessages(){
			let firstName = prompt("Enter your First Name: ");
			let lastName = prompt("Enter your Last Name: ");

			console.log("Hello " + firstName + " " + lastName);
		}
		printWelcomeMessages();

// [Section] Function Naming Convention
	
	// function names should be definitive of the task it will perform. Usually conatins a verb.

	function getCourse(){
		let course = ["Science 101", "Math 101", "English 101"];

		console.log(course);
	}
	getCourse();

	// Avoid generic, pointless and inappropriate function names to avoid confusion within your code/program.
	// name your function in small caps. Use camelCasing as your naming convention
		function booTwahz(){
			let name = "Jamie";
			console,log(name);
		}
		getName();